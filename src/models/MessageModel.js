var util = require('util')
  , underscore = require('underscore')  
  , crypto = require('crypto')  
  , superClass = require('../components/ActiveRecord.js');

function MessageModel(scenario) {
  superClass.call(this, scenario);
  
  this.setAttributes({
    user : null,
    room : null,
    content: null,
	time : (new Date()).toString()
  });
}

util.inherits(MessageModel, superClass);

MessageModel.prototype.rules = function() {
  return [
    {
      rule : 'required',
      attributes: ['user', 'room', 'content']
    }
  ];
}

MessageModel.prototype.tableName = function() {
  return 'message';
}

MessageModel.model = function() {
  return MessageModel.super_.model(MessageModel);
}

MessageModel.prototype.beforeValidate = function(callback) {
  callback(true);
}

module.exports = MessageModel;