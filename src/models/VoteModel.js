var util = require('util')
  , underscore = require('underscore')  
  , crypto = require('crypto')  
  , superClass = require('../components/ActiveRecord.js');

function VoteModel(scenario) {
  superClass.call(this, scenario);
  
  this.setAttributes({
    room : null,
	user : null,	
	value : 0
  });
}

util.inherits(VoteModel, superClass);

VoteModel.prototype.rules = function() {
  return [
    {
      rule : 'required',
      attributes: ['room', 'user', 'value']
    }
  ];
}

VoteModel.prototype.tableName = function() {
  return 'vote';
}

VoteModel.model = function() {
  return VoteModel.super_.model(VoteModel);
}

module.exports = VoteModel;