var util = require('util')
  , underscore = require('underscore')  
  , crypto = require('crypto')  
  , superClass = require('../components/ActiveRecord.js');

function UserModel(scenario) {
  superClass.call(this, scenario);
  
  this.setAttributes({
    firstname : null,
    lastname : null,
    surname : null,
    dob : null,   
    type : null,
    gender : null,
    login : null,
    email : null,
    password : null,
    salt : null,
    rating : 0,
    coins : 0
  });
}

util.inherits(UserModel, superClass);

UserModel.prototype.rules = function() {
  return [
    {
      rule : 'required',
      attributes: ['email']
    }
  ];
}

UserModel.prototype.tableName = function() {
  return 'user';
}

UserModel.model = function() {
  return UserModel.super_.model(UserModel);
}

UserModel.prototype.isRoomOwner = function(room) {	
	return room.getAttributes('user')._id.toString() == this.getAttributes('_id').toString();
}

UserModel.prototype.beforeValidate = function(callback) {   
  if (this.getScenario() == 'insert' && this.getAttributes('password')) {  
    var salt = Math.random() + '';
    this.setAttributes({
      salt : salt,
      password : UserModel.hashPassword(this.getAttributes('password'), salt)
    });
  }
  callback(true);
}

UserModel.hashPassword = function(password, salt) {	   
	return crypto.createHmac('sha1', salt).update(password).digest('hex');
}

UserModel.prototype.validPassword = function(password) {
  return this.getAttributes('password') === UserModel.hashPassword(password, this.getAttributes('salt'));
}

module.exports = UserModel;