var util = require('util')
  , underscore = require('underscore')  
  , crypto = require('crypto')  
  , superClass = require('../components/ActiveRecord.js');

function RoomModel(scenario) {
  superClass.call(this, scenario);
  
  this.setAttributes({
    name : null,
	stream : null,
	user : null
  });
}

util.inherits(RoomModel, superClass);

RoomModel.prototype.rules = function() {
  return [
    {
      rule : 'required',
      attributes: ['name']
    }
  ];
}

RoomModel.prototype.tableName = function() {
  return 'room';
}

RoomModel.model = function() {
  return RoomModel.super_.model(RoomModel);
}

module.exports = RoomModel;