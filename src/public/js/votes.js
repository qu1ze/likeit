var chart, chartsData;

function initCharts(data) {
   
	var chartsData = {
		chart: {
			renderTo: 'choices',
			type: 'bar',
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false
		},
        title: {
            text: 'Рейтинг'
        },
        xAxis: {
            categories: ['Users'],
            labels: {
                enabled: false
            },
            title: {
                text: null
            },
            lineColor: 'transparent',
        },
        yAxis: {
            min: 0,
            max: data.dislikes+data.likes,
            endOnTick: false,
            gridLineWidth: 0,
            minorGridLineWidth: 0,
            lineColor: 'transparent',
            labels: {
                enabled: false
            },
            title: {
                text: null
            },
            minorTickLength: 0,
            tickLength: 0
        },
        legend: {
            reversed: true
        },
        tooltip: false,
        plotOptions: {
            series: {
                stacking: 'normal'
            },
            bar: {
                dataLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    },
                    formatter: function() {
                        if (!this.y) {
                            return null;
                        }
                        return this.y;
                    }
                }
            }
        },
		series: [{
			name: 'Не нравится',
			data: [data.dislikes],
            color: '#cc3333'
		},{
			name: 'Нравится',
			data: [data.likes],
            color: '#ace839'
		}]
	};
	chart = new Highcharts.Chart(chartsData);
}

function updateCharts(data) {
	chart.series[1].setData([data.likes]);
	chart.series[0].setData([data.dislikes]);
    chart.yAxis[0].setExtremes(0,data.dislikes+data.likes);
    chart.redraw();
}

socket.emit('init charts', { user : fullUser._id, room : room });
socket.on('got init charts', function(result) {
	initCharts(result.data);
	if (result.userVote) {			
		if (result.userVote == 1) {
			$('#likeSpan').toggleClass('fa-thumbs-up');
			$('#like').attr('checked', 'checked');
			//$('#dislike').attr('disabled', 'disabled');
		} else {
			$('#dislikeSpan').toggleClass('fa-thumbs-down');
			$('#dislike').attr('checked', 'checked');
			//$('#like').attr('disabled', 'disabled');
		}
	}
});

socket.on('charts update', function (data) {		
    updateCharts(data);
});

$('#like').on('click', function() {
	var self = $(this);
	if (self.is(':checked')) {
		socket.emit('upVote', { room : room, user : fullUser._id });
		if ($('#dislike').is(':checked')){
			$('#dislikeSpan').toggleClass('fa-thumbs-down');
			$('#dislike').removeAttr('checked');
		}
	} else {
		socket.emit('unVote', { room : room, user : fullUser._id });
	}
	$('#likeSpan').toggleClass('fa-thumbs-up');
});
	
$('#dislike').on('click', function() {
	var self = $(this);
	if (self.is(':checked')) {
		socket.emit('downVote', { room : room, user : fullUser._id });
		if ($('#like').is(':checked')){
			$('#likeSpan').toggleClass('fa-thumbs-up');
			$('#like').removeAttr('checked');
		}
	} else {
		socket.emit('unVote', { room : room, user : fullUser._id });
	}
	$('#dislikeSpan').toggleClass('fa-thumbs-down');
});