var chart, chartsData;

function initCharts(data) {
	var chartsData = {
        chart: {
            renderTo: 'records',
            type: 'bar'
        },
        title: {
            text: 'Рекорды'
        },
        xAxis: {
            categories: data.names,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Рекорды',
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: false,
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: false,
        series: [{
            name: '',
            data: data.records
        }]
    };
	chart = new Highcharts.Chart(chartsData);
}
initCharts({names: ['йцу','йцу2','йцу3', 'йцу4','йцу5','йцу6','йцу7', 'йцу8'], records: [567,456,345,234,123,89,45,12], });
socket.emit('init records', { user : fullUser._id, room : room });
socket.on('got init records', function(data) {
	initCharts(data);
});