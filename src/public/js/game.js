var interval, preInterval;

function cleanGame() {
	clearInterval(interval);	
	clearInterval(preInterval);	
	$('#timer').text(GAME_TIMER);
	$('#keyword').val('');
	$('#keyword').removeAttr('disabled');
	$('#start').removeAttr('disabled');
}	

function finishGame(message) {
	cleanGame();
	socket.emit('end game', {room: room, message: message});
}

socket.on('game ended', function(data) {	
	cleanGame();
	putInfoMessage(data.message, 'win');
});
socket.on('game started', function() {	
	$('.msg-wrap').html('');
	putInfoMessage('Игра началась', 'start');
});

function startGame(message) {
	socket.emit('start game', {user: fullUser, room: room, keyword: message});	
	
	
	interval = setInterval(function() {		
		var value = parseInt($('#timer').text()) - 1;
		if (value > -1) {
			$('#timer').text(value);
		} else {
			putInfoMessage('Игра закончена. Для продолжения игры выберите новое слово и нажмите "Старт"','info');			
		}
	}, 1000);
}

$('#start').on('click', function() {
	var message = $('#hiddenKeyword').text($('#keyword').val()).html();	
	
	if (!message) {
		alert('Пожалуйста введите ключевое слово');
		return false;
	}
	$('#keyword').attr('disabled', 'disabled');
	$('#start').attr('disabled', 'disabled');
	preInterval = setInterval(function() {
		var value = parseInt($('#timer').text()) - 1;
		if (value > -1) {
			$('#timer').text(value);
		} else {
			clearInterval(preInterval);
			$('#timer').text(TIMER_PERIOD);
			startGame(message);				
		}
	}, 1000);
		
	return false;
});

$('#keyword').on('keydown', function(e) {		
	if (!e.shiftKey && e.keyCode == 13) {
		$('#start').click();			
		return false;
	}
});

socket.on('message success', function(data) {

	if (fullRoom.user._id == fullUser._id && data.user._attributes._id != fullRoom.user._id) {
		socket.emit('check answer', data);		
	}
});

putInfoMessage = function(message, type){
	$('#notification').text(message);
	$('#notification').removeAttr('class');
	if ( type == 'win' ) {
		$('#notification').addClass('alert alert-success');
	} else if ( type == 'start' ) {
		$('#notification').addClass('alert alert-success');
	} else if ( type == 'info' ) {
		$('#notification').addClass('alert alert-info');
	} else if ( type == 'error') { 
		$('#notification').addClass('alert alert-danger');
	}
	console.log(message, type);
}
