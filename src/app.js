var express = require('express')
  , http = require('http')
  , path = require('path')
  , st = require('st')
  , nconf = require('./config').nconf
  , config = require('./config').config
  , mongodb = require('mongodb')
  , passport = require('passport')
  , favicon = require('serve-favicon')
  , morgan  = require('morgan')
  , bodyParser = require('body-parser')
  , cookieParser = require('cookie-parser')
  , session = require('express-session')
  , flash = require('connect-flash')  
  , expressValidator = require('express-validator')
  , MongoStore = require('connect-mongo')({ session : session })
  , tmplEngine = require('ejs-locals-improved')  
  , MongoClient = mongodb.MongoClient;   

function run(hasDb) {
	
	function main(db) {
		var app = express();
							
		app.set('port', process.env.PORT || nconf.get('app:port'));
		app.set('views', path.join(__dirname, nconf.get('app:view:folder')));
		app.set('view engine', nconf.get('app:view:engineExt'));
		app.engine('html', tmplEngine);					
		
		app.use(favicon(path.join(__dirname, nconf.get('app:webRoot'), 'favicon.ico')));
		app.use(morgan('dev'));
		app.use(bodyParser());		
		app.use(expressValidator(config.expressValidatorConfig));
			app.use(cookieParser('your secret here'));		
				
			if (db) {
				app.set('mongoDb', db);		
				global.mongoDb = db;
				app.use(session({
					secret : 'secret_session_phrase',
					cookie : {
						maxAge : nconf.get('app:cookie:maxAge')
					},
					store : new MongoStore({
						db : nconf.get('db:name')
					})
				}));
			} else {
				app.use(session());
			}
		
		/*
		 * Passport section
		 */
		require('./components/Passport.js')(passport);
		app.use(passport.initialize());
		app.use(passport.session());
		app.use(flash());						
		
		//installing all necessary variables for layout
		app.use(function(req, res, next) {			
		  config.registerAppLocals(req, res.locals);
				next();
			});
		
		console.log('Environment: ' + app.get('env'));
			if ('development' == app.get('env')) {
				app.use(require('errorhandler')());
			}
			
		var server = http.createServer(app);
		var io = require('socket.io').listen(server);
				
		require('./components/Bootstrap.js')(app, io);
		
		/* Attach public asset folders */
		config.publicAssets.forEach(function(extraPath) {       
		  app.use(st({
			path: path.join(__dirname, extraPath),
			url: '/',
			cache: false,
			passthrough: true
		  }));
		});
		
		
		server.listen(app.get('port'), function(){
			console.log('Express server listening on port ' + app.get('port'));
		});					 
		
	}	

	if (hasDb) {
		MongoClient.connect(nconf.get('db:connectionString'), function(mongoConnectionError, db) {
			if (mongoConnectionError) throw mongoConnectionError;
					
			main(db);
		});
	} else {
		main();
	}
}

run(nconf.get('db'));



