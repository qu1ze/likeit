var Contract = require('./../components/Contract')
  , underscore = require('underscore');

module.exports = underscore.extend({
  validate : function(req, callback) {
    req.checkBody('email', 'Email is required').notEmpty().isEmail();
    req.checkBody('password', 'Password is required').notEmpty();
    req.checkBody('passwordConfirmation', 'Confirmation password is required').notEmpty().equals(req.body.password);
    
    callback( Contract.errorResponse(req.validationErrors()) );
  }  
}, Contract);