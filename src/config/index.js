var nconf = require('nconf')
  , path = require('path');
 
nconf.env().argv();

nconf.file(path.join(__dirname, 'config.json'));
nconf.file('local', path.join(__dirname, 'config.local.json'));

var frontendDir = nconf.get('app:frontendRoot');

module.exports = {
  nconf : nconf,
  config : {
    publicAssets : [
      'public',
      '../' + frontendDir,
      '../' + frontendDir + '/assets'
    ],
    expressValidatorConfig : {
      errorFormatter: function(param, msg, value) {
        var namespace = param.split('.')
        , root = namespace.shift()
        , formParam = root
        , error = {};
     
        while(namespace.length) {
          formParam += '[' + namespace.shift() + ']';
        }                       
        error[formParam] = [ msg ];        
        return { error : error };
      }
    },
    registerAppLocals : function(req, locals) {
      locals.title = nconf.get('app:title');
      req.app.locals.open = nconf.get('app:view:open') || '<%';
      req.app.locals.close = nconf.get('app:view:close') || '%>';
    }
  }
};