var underscore = require('underscore')    
  , express = require('express')
  , passport = require('passport')
  , mongodb = require('mongodb')
  , underscore = require('underscore')
  , RoomModel = require('./../models/RoomModel')
  , suCtrl = require('../components/Controller.js');  

var router = express.Router()
  , prefix = '/room';

router.use(function(req, res, next) {
	if (req.isAuthenticated()) {
		next();
	} else {
		res.redirect('/');
	}	
});
  
router.route('/create')
	.get(function(req, res) {
		res.render('create', {
			user : req.user
		});
	})
	.post(function(req, res) {
		var model = new RoomModel;
		model.setAttributes(underscore.extend(req.body, { user : new mongodb.DBRef('user', req.user.getAttributes('_id')) }));		
		model.save(function(err, result) {
			if (err) { 
				return res.render('create', {
					user : req.user
				});
			}
			res.redirect('/');
		});
	});

router.get('/get/:id', function(req, res) {	
	RoomModel.model().fetch(['user']).findByPk(req.params.id, function(err, data) {
		if (err) {return res.redirect('/'); }
		
		res.render('get', {
			user : req.user,
			room : data,
			game : {
				timer : 5,
				period : 60
			},
            js: [
				{path: '/js/init.js'},				
				{path: '/js/main.js'},				
				{path: '/js/votes.js'},							
				{path: '/js/game.js'},				
			]
		});
	});	
});

router.route('/edit/:id')
	.get(function(req, res) {	
	
		RoomModel.model().fetch(['user']).findByAttributes({ _id : new mongodb.ObjectID(req.params.id), 'user.$id' : req.user.getAttributes('_id')}, function(err, data) {
			if (err || !data) {return res.redirect('/'); }
			
			res.render('edit', {
				user : req.user,
				room : data
			});
		});	
	})
	.post(function(req, res) {
		
		RoomModel.model().findByPk(req.params.id, function(err, room) {
			room.setAttributes(req.body);
			room.save(function(err, result) {
			if (err) { 
					return res.render('edit', {
						user : req.user
					});
				}
				res.redirect('/');
			});
			
		});
				
	});

router.get('/delete/:id', function(req, res) {
	RoomModel.model().deleteByPk(req.params.id, function(err, removedItems) {
		if (err) { return res.redirect('/'); }
		
		res.redirect('/');
	});
});
	
router.get(suCtrl.unitePath(prefix, '/index'), function(req, res) {
  res.render('index');
});

module.exports = {  
  instance : router ,
  prefix : prefix
};
