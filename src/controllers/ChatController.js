var underscore = require('underscore')    
  , express = require('express')
  , passport = require('passport')
  , async = require('async')
  , suCtrl = require('../components/Controller.js')
  , MessageModel = require('../models/MessageModel')
  , UserModel = require('../models/UserModel')
  , wordUtils = require('./../helpers/WordUtils')
  , mongodb = require('mongodb');

var router = express.Router()
  , prefix = '/chat'
  , users = {}
  , secretKeyWord;

function validDate(number) {
	if (number < 10) {
		return '0' + number;
	}
	
	return number;
}  
  
router.inject = function(io) {
    io.sockets.on('connection', function (socket) {
		
		function updateUsers(room) {
			io.sockets.in(socket.room).emit('usernames', { users : users, room : room});
		}
		
		socket.on('room', function(data) {			
			socket.join(data.room);
			socket.room = data.room;
			socket.emit('roomed', { room : data.room });
		});
		
		socket.on('wire user', function(data) {
			socket.userId = data.user._id;
			socket.room = data.room;
			socket.userId = data.user._id;	
			if (!users[socket.room]) {
				users[socket.room] = {};
			}
			 
			users[socket.room][socket.userId] = data.user;			
			updateUsers(socket.room);
		});
		
        socket.on('message', function (data) {
            
            UserModel.model().findByPk(data.user._id, function(err, user) {				
                if (err) {
                    socket.emit('message error', {
                        data: data
                    });
                } else {
					var model = new MessageModel();
					data = underscore.extend(data, {
						room : new mongodb.DBRef('room', new mongodb.ObjectID(data.room)),
						user : new mongodb.DBRef('user', new mongodb.ObjectID(data.user._id)),
						time : Date.now()
					});
                    model.setAttributes(data);
                    model.save(function(err, result) {
                        if (err) {
                            socket.emit('message error', {
                                data: data
                            });
                        } else {    
							var date = new Date(data.time);
                            io.sockets.in(socket.room).emit('message success', {                                                                                              
                                content: data.content,
                                time : date.getHours() + ':' + ( validDate(date.getMinutes()) ) + ':' + validDate(date.getSeconds()) ,
                                firstname: user ? user.getAttributes('firstname') : 'unnamed',
                                lastname: user ? user.getAttributes('lastname') : 'unnamed',
								user : user
                            });
                        }
                    });
                }
            });


        });
		
		socket.on('init messages', function(data) {
			MessageModel.model().fetch(['user']).findAllByAttributes({ 'room.$id' : new mongodb.ObjectID(data.room) }, function(err, messages) {		
				console.log(data);
				console.log(messages);
				io.sockets.in(socket.room).emit('got init messages', {
					messages : messages					
				});				
			});					
		});
		
		socket.on('start game', function(data) {
			secretKeyWord = data.keyword;
			io.sockets.in(socket.room).emit('game started');
		});
		
		socket.on('check answer', function(data) {
			if (wordUtils.probablyEqual(data.content, secretKeyWord)) {
				io.sockets.in(socket.room).emit('game ended', { message : 'Игра закончена. Правильный ответ: ' + secretKeyWord + '. Победитель: ' + data.firstname + ' ' + data.lastname });
			}
		});
		
		socket.on('end game', function(data) {
			secretKeyWord = null;			
			MessageModel.model().findAllByAttributes({ 'room.$id' : new mongodb.ObjectID(data.room) }, function(err, items) {
				async.each(items, function(item, cb) {
					MessageModel.model().deleteByPk(item.getAttributes('_id'), function(err, removed) {
						if (removed) {
							cb();
						} else {
							cb('Not removed');
						}
					});
				}, function(err) {
					console.log(err);
					io.sockets.in(socket.room).emit('game ended', { message : data.message });	
				});
			});
		});
		
		socket.on('disconnect', function() {
			console.log('DISCONNECT');
			delete users[socket.room][socket.userId];
			updateUsers(socket.room);
		});
    });
}

module.exports = {  
  instance : router ,
  prefix : prefix
};
