var underscore = require('underscore')    
  , express = require('express')
  , passport = require('passport')
  , UserContract = require('./../contracts/UserContract')
  , UserModel = require('./../models/UserModel')
  , RoomModel = require('./../models/RoomModel')
  , suCtrl = require('../components/Controller.js');  

var router = express.Router()
  , prefix = '/site';

router.get(suCtrl.unitePath(prefix, '/index').concat(['/', prefix]), function(req, res) {  
	var user = req.user;
	if (user) {
		RoomModel.model().fetch(['user']).findAll(function(err, rooms) {
			if (err) { return res.render('index', { user : user, rooms : [] }); }	
				
			res.render('index', {		
				user : user,
				rooms : rooms
			});			
		});			
	} else {
		res.render('index', {	
			rooms : [],
			user : user
		});			
	}
});

router.get(suCtrl.unitePath(prefix, '/logout'), function(req, res) {
  req.logout();
	res.redirect('/');
});

router.get('/signin', function(req, res) {
	res.render('signin', {
		user : req.user,
		errors : []
	});
});

router.get('/top', function(req, res) {
    res.render('top', {
        user : req.user,
        js: [
            {path: '/js/records.js'},
        ]
    });
});

router.post(suCtrl.unitePath(prefix, '/login'), function(req, res) {	
	passport.authenticate('local', { 
		successRedirect: '/',
		failureRedirect: '/',
		failureFlash: true 
	})(req, res);
});

router.route(suCtrl.unitePath(prefix, '/signup'))
  .get(function(req, res) {
    res.render('signup', {
      errors : null,
	  user : req.user
    });
  })
  .post(function(req, res) {
    UserContract.validate(req, function(errors) {
      if (errors) {        
        return res.render('signup', {
          errors : UserContract.errorResponse(errors),
		  user : req.user
        });
      }
      
      var user = new UserModel;
      user.setAttributes(req.body);
      user.save(function(err, result) {
        if (err) {             
          res.render('signup', {
            errors : UserContract.errorResponse(err),
			user : req.user
          });
        } else {
          res.redirect('/');
        }
      });
      
    });                 
  });

module.exports = {  
  instance : router
};
