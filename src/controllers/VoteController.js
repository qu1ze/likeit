var underscore = require('underscore')    
  , express = require('express')
  , passport = require('passport')
  , mongodb = require('mongodb')
  , VoteModel = require('./../models/VoteModel')  
  , suCtrl = require('../components/Controller.js');  

var router = express.Router()
  , prefix = '/vote';

function getVotes(callback, data) {
	VoteModel.model().aggregate([
		{ $match : { value : 1, room : data.room } },
		{
			$group : {
				_id : null,
				count : { $sum : '$value' }
			}			
		}				
		], function(err, likes) {
		VoteModel.model().aggregate([
			{ $match : { value : -1, room : data.room } },
			{
				$group : {
					_id : null,
					count : { $sum : '$value' }
				}			
			}				
		], function(err, dislikes) {				
			callback({
				likes : likes.length ? likes[0]['count'] : 0,
				dislikes : dislikes.length ? dislikes[0]['count'] * -1 : 0
			});
		});
	});
}

router.inject = function(io) {
	io.sockets.on('connection', function (socket) {	
		socket.on('room', function(data) {
			socket.join(data.room);
			socket.room = data.room;				
		});
		
		socket.on('upVote', function (data) {	
			VoteModel.model().deleteAllByAttributes({ 'user' : data.user, room : data.room }, function(err, removedItems) {		
				var vote = new VoteModel;
				vote.setAttributes({
					value : 1,
					room : data.room,
					user : data.user
				});
				vote.save(function(err, result) {
					getVotes(function(data) {
						io.sockets.in(socket.room).emit('charts update', data);
					}, data);	
				});
			});
		});
		
		socket.on('downVote', function (data) {
			VoteModel.model().deleteAllByAttributes({ user : data.user, room : data.room }, function(err, removedItems) {
				var vote = new VoteModel;
				vote.setAttributes({
					value : -1,
					room : data.room,
					user : data.user
				});
				vote.save(function(err, result) {
					getVotes(function(data) {
						io.sockets.in(socket.room).emit('charts update', data);
					}, data);	
				});
			});
		});
		
		socket.on('unVote', function (data) {					
			VoteModel.model().deleteAllByAttributes({ 'user' : data.user, room : data.room }, function(err, removedItems) {					
				getVotes(function(data) {
					io.sockets.in(socket.room).emit('charts update', data);
				}, data);	
			});
		});
		
		socket.on('init charts', function(data) {
			VoteModel.model().findByAttributes({ user : data.user, room : data.room }, function(err, vote) {
				getVotes(function(votes) {
					io.sockets.in(socket.room).emit('got init charts', {
						data : votes,
						userVote : vote ? vote.getAttributes('value') : null
					});
				}, data);	
			});					
		});
		
		socket.on('disconnect', function() {
			console.log('DISCONNECT');				
		});
	});
}

module.exports = {  
  instance : router ,
  prefix : prefix
};
