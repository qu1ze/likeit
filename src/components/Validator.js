var ModelError = require('./errors/ModelError.js')    
  , TypeError = require('./errors/TypeError.js')
  , underscore = require('underscore')
  , path = require('path')
  , async = require('async')
  , Model = require('../components/Model.js');

function Validator(ruleParams) {
    this.ruleParams = ruleParams ? ruleParams : {};
    this.attributes = ruleParams.attributes ? ruleParams.attributes : {};
    this.message = ruleParams && ruleParams.message ? ruleParams.message : '';
	this.on = ruleParams.on && ruleParams.on.length ? ruleParams.on : [];
	this.except = ruleParams.except && ruleParams.except.length ? ruleParams.except : [];		
}

Validator.builtInValidators = {
	required : 'RequiredValidator',
	integer : 'IntegerValidator',
	numeric : 'NumericValidator',
	inrange : 'InRangeValidator'
};

Validator.createValidator = function(key, object, rule) {
	var validator = {};
	
	if (Validator.builtInValidators[key]) {			
		validator = new (require(path.join(__dirname, 'validators', Validator.builtInValidators[key] + '.js')))(rule);		
	} else if (object[key]) {
		validator = new (require(path.join(__dirname, 'validators' , 'InlineValidator.js')))(rule);		
		validator.method = key;		
	}
	
	validator.on = rule.on ? rule.on : [];
	validator.except = rule.except ? rule.except : [];
	
	return validator;
}

Validator.createValidators = function(rules, object) {	
	var results = [];
	
	underscore.each(rules, function(rule, key) {					
		results.push(Validator.createValidator(rule.rule, object, rule));
	});
		
	return results;
}

Validator.prototype.applyTo = function(scenario) {	
	if (~this.except.indexOf(scenario))
		return false;
	
	return !this.on.length || ~this.on.indexOf(scenario);
}

Validator.prototype.validate = function(model, next) {
	var self = this
	  , modelAttributes = model.getAttributes();
	  
	async.eachSeries(this.attributes, function(attribute, callback) {	
		if (!(attribute in modelAttributes))
			self.addError(model, attribute);
		
		self.validateAttribute(model, attribute, callback);		
	}, function(err) {
		if (err) {
			self.addError('system', 'Validation system error');
			return false;
		}		
		next();
	});	  
}

Validator.prototype.addError = function(model, attribute) {	
	var message = this.message;
	if (!message)
		message = this.getMessage(attribute);
	
	model.addError(attribute, message);
}

Validator.prototype.validateAttribute = function(attribute) {
    throw new Error('Abstract Method');
}

Validator.prototype.getMessage = function(attribute) {
    return attribute + ' is not valid!';
}

module.exports = Validator;