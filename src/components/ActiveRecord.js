var superClass = require('./Model.js')
  , ModelError = require('./errors/ModelError.js')
  , mongo = require('mongodb')
  , async = require('async')
  , underscore = require('underscore')
  , util = require('util');

function ActiveRecord(scenario) {
	superClass.call(this, scenario);	
}

util.inherits(ActiveRecord, superClass);

ActiveRecord._models = [];

ActiveRecord.model = function(modelName) {
	
	if (ActiveRecord._models[modelName])	
		return ActiveRecord._models[modelName];	
	else	
		return ActiveRecord._models[modelName] = new modelName;		 
}

ActiveRecord.prototype.tableName = function() {
	return null;
}

ActiveRecord.prototype.fetch = function(fields) {
  this.fetches = fields;
  return this;
}

ActiveRecord.prototype.filter = function(result, callback) {
  var self = this;
   
  if (self.fetches && self.fetches.length) {          
    async.each(self.fetches, function(field, callback) {
      self.getConnection().dereference(result[field], function(err, item) {
        if (err) { return callback(err, item); }
        
        result[field] = item;
        callback();
      });
    }, function(err) {
      callback(err, result);
    });           
  } else {
    callback(null, result);
  }   
}

ActiveRecord.prototype.find = function(selector, callback, options) {
  var self = this
	  , options = options || {};
	  
	this.getConnection()
		.collection(this.tableName())
		.find(selector, options)		
		.toArray(function(err, result) {
			if (err) { return callback(err, result); }
			
			self.afterFind(result, function(err, afterResult) {				
        callback(err, afterResult);
      });				
		});
}

ActiveRecord.prototype.findAll = function(callback, options) {
	
	var self = this
	  , options = options || {};
	  
	this.getConnection()
		.collection(this.tableName())
		.find({}, options)		
		.toArray(function(err, result) {
			if (err) { return callback(err, result); }
			
			self.afterFind(result, function(err, afterResult) {				
        callback(err, afterResult);
      });				
		});
}

ActiveRecord.prototype.findByAttributes = function(attributes, callback, options) {
		
	var self = this
	  , options = options || {};
	
	this.getConnection()
		.collection(this.tableName())				 
		.findOne(attributes, options, function(err, result) {
			if (err) { return callback(err, result); }				
			   
      self.afterFind(result, function(err, afterResult) {        
        callback(err, afterResult);        
      });				          		
		});				
}

ActiveRecord.prototype.findAllByAttributes = function(attributes, callback, options) {
	
	var self = this
	  , options = options || {};
	
	this.getConnection()
		.collection(this.tableName())
		.find(attributes, options)		
		.toArray(function(err, result) {
			if (err) { return callback(err, result); }
			
			self.afterFind(result, function(err, afterResult) {
        callback(err, afterResult);
      });			
		});
}

ActiveRecord.prototype.findByPk = function(key, callback, options) {
	if (typeof key == 'string')
		this.findByAttributes({_id : mongo.BSONPure.ObjectID.createFromHexString(key)}, callback, options);
	else
		this.findByAttributes({_id : key}, callback, options);
}

ActiveRecord.prototype.afterFind = function afterFind(result, callback) {
  var self = this;
	if (underscore.isEmpty(result)) { return callback(null, result); }
			
	var attributesToInstance = (function(attributeValues) {		
		var result = new this.constructor;
		result.setAttributes(attributeValues);	
		return result;
	}).bind(this);
	
	if(!(result instanceof Array)) {	    
    self.filter(result, function(fitlerError, filteredResult) {
      if (fitlerError) { return callback(fitlerError, filteredResult); }
        
      callback(fitlerError, attributesToInstance(filteredResult));
    });     		
	} else {  
    async.map(result, function(resultItem, callback) {
      self.filter(resultItem, function(fitlerError, filteredResult) {
        if (fitlerError) { return callback(fitlerError, filteredResult); }
          
        callback(fitlerError, attributesToInstance(filteredResult));
      });            
    }, function(err, result) {      
      callback(err, result);     
    });    
	}
  
  this.cleanInstance();
}

ActiveRecord.prototype.cleanInstance = function() {
  //clean fetches
  this.fetches = [];
}

ActiveRecord.prototype.aggregate = function(data, callback, options) {
	
	var data = data || []
	  , options = options || {};	  
	
	this.getConnection()
		.collection(this.tableName())
		.aggregate(data, options, function(err, results) {
			if (err) { return callback(err, results); }
			
			callback(err, results);
		});
}

ActiveRecord.prototype.distinctAll = function(key, callback) {
	this.getConnection()
		.collection(this.tableName())
		.distinct(key, function(err, results) {
			if (err) { return callback(err, results); }
			
			callback(null, results);
		});
}

ActiveRecord.prototype.distinctAllByAttributes = function(key, query, callback, options) {
	var query = query || {}
	  , options = options || {};
	  
	this.getConnection()
		.collection(this.tableName())
		.distinct(key, query, options, function(err, results) {
			if (err) { return callback(err, results); }
			
			callback(null, results);
		});
}

ActiveRecord.prototype.countAll = function(callback) {		
	this.getConnection()
		.collection(this.tableName())
		.count(function(err, count) {
			if (err) { return callback(err, count); }
			
			callback(null, count);
		});
}

ActiveRecord.prototype.countAllByAttributes = function(query, callback, options) {
	var query = query || {}
	  , options = options || {};
	
	this.getConnection()
		.collection(this.tableName())
		.count(query, options, function(err, count) {
			if (err) { return callback(err, count); }
			
			callback(null, count);
		});
}

ActiveRecord.prototype.save = function(callback, data, options) {
	// This array can be an array of user objects
	var data = data || []
	  , self = this
	  , options = underscore.extend(options || {}, {
			runValidation : options && options.runValidation !== undefined ? options.runValidation : true 
		});
	
	//option runValidation can prevent validation	
	self.validate(function(validationResult) {
		if (!options.runValidation || validationResult) {
			if (id = self.getAttributes('_id'))	
				self.update({_id : id}, data, callback, options);
			else
				self.insert(data, callback, options);
		} else {
			callback(self._errors);	
		}
	});	
}

ActiveRecord.prototype.beforeSave = function(callback) {
	callback(true);
}

ActiveRecord.prototype.afterSave = function(callback) {
	callback(true);
}

ActiveRecord.prototype.update = function(selector, data, callback, options) {
	var data = !underscore.isEmpty(data) ? data : this.getAttributes()  
	  , updateOptions = { multi : true }
	  , options = options ? underscore.extend(options, updateOptions) : updateOptions
	  , _id = '';	
	 
	// _id can not be in $set data
	if ('_id' in data) {
		_id = data._id;
		delete data._id; 
	}
	 
	this.getConnection()
		.collection(this.tableName())
		.update(selector, { $set : data }, options, function(err, result) {		
			if (err) { return callback(err, result); }
			
			//restore _id
			if (_id)
				data._id = _id;
				
			callback(null, result);
		});			
}

ActiveRecord.prototype.insert = function(data, callback, options) {	
	
	var self = this;	  
	this.getConnection()
		.collection(this.tableName())
		.insert(!underscore.isEmpty(data) ? data : this.getAttributes(), options, function(err, result) {		
			if (err) { return callback(err, result); }
			
			self.setScenario('update');
			callback(null, result);
		});
}

ActiveRecord.prototype.deleteAll = function(callback, options) {	
	var options = options ? options : {};
	  
	this.getConnection()
		.collection(this.tableName())
		.remove({}, options, function(err, removedItems) {
			if (err) { return callback(err, removedItems); }
			
			callback(null, removedItems);
		});
}

ActiveRecord.prototype.deleteAllByAttributes = function(attributes, callback, options) {
	var attributes = attributes ? attributes : {}
	  , options = options ? options : {};
	  
	this.getConnection()
		.collection(this.tableName())
		.remove(attributes, options, function(err, removedItems) {
			if (err) { return callback(err, removedItems); }
			
			callback(null, removedItems);
		});
}

ActiveRecord.prototype.deleteByAttributes = function(attributes, callback, options) {
	var attributes = attributes ? attributes : {}
	  , singleOption = { single : true }
	  , options = options ? underscore.extend(options, singleOption) : singleOption;
	  
	this.getConnection()
		.collection(this.tableName())
		.remove(attributes, options, function(err, removedItems) {
			if (err) { return callback(err, removedItems); }
			
			callback(null, removedItems);
		});
}

ActiveRecord.prototype.deleteByPk = function(key, callback, options) {    
	if (typeof key == 'string') {    
		this.deleteByAttributes({_id : mongo.BSONPure.ObjectID.createFromHexString(key)}, callback, options);
	} else {
		this.deleteByAttributes({_id : key}, callback, options);
  }
}

module.exports = ActiveRecord;