var express = require('express')
  , path = require('path')    
  , superCtrl = require('./Controller.js')
  , underscore = require('underscore')  
  , fs = require('fs');

function parse(initPath, application, io) {

  fs.readdirSync(initPath).forEach(function(name) {
	
    var itemPath = path.join(initPath, name)
      , stat = fs.statSync(itemPath);

    if (stat && stat.isDirectory(itemPath)) {
              
      //recursive dir reading
      parse(itemPath, application);

    } else {         
      var controller = require(itemPath)        
        , controllerName = controller.name || name
		, app = express()
        , viewFolderName = controllerName.toLowerCase().substring(0, controllerName.indexOf('Controller'));					
      
	  
	  controller.instance.inject && controller.instance.inject(io);
	  	
      // connecting views	  
      app.set('views', path.join(__dirname, '..', 'views', viewFolderName));
      
      app.use(controller.prefix || superCtrl.prefix, controller.instance);       
	  
	  application.use(app);
    }
    
	});		
}  

module.exports = function(application, io) {	
	parse(path.join(__dirname, '..', 'controllers'), application, io);	
}