module.exports = {
  prefix : '/',
  
  isAuthenticated : function(req, res, next) {
    if (req.isAuthenticated())
      return next();
    else 
      res.redirect('/');
  },
  
  unitePath : function(prefix, path) {
    return [prefix + path, path];
  }
};