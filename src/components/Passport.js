var LocalStrategy = require('passport-local').Strategy  
  , underscore = require('underscore')
  , nconf = require('./../config').nconf
  , UserModel = require('./../models/UserModel');

module.exports = function( passport ) {

  passport.serializeUser(function(user, done) {     
    done(null, user.getAttributes('_id'));
  });

  passport.deserializeUser(function(id, done) {      
    UserModel.model().findByPk(id, function(err, user) {      
      if (err) { return done(err) };
      done(null, user);   
    });    
  });

  passport.use(new LocalStrategy({
      usernameField : 'email',
      passwordField : 'password'
    },
    function(email, password, done) {  
      UserModel.model().findByAttributes({ email : email }, function(err, user) {
        if (err) { return done(err); }
        
        if (!user) {
          return done(null, false, { message : 'Incorrect email'});
        }        
        if (!user.validPassword(password)) {
          return done(null, false, { message : 'Incorrect password'});
        }        
        done(null, user);
      });      
    }
  ));
    
  
  
};