var spawn = require('child_process').spawn
  , config = require('../config')
  , path = require('path')
  , debug = config.get('DEBUG')
  , fs = require('fs');

function _updateHtml(callback) {			
	
	var reportPath = path.join(__dirname, '../' + config.get('app:testReportsFolder'), 'report.html');
	
	fs.exists(reportPath, function(exists) {						
		if (exists) {			
			
			debug && console.log('Old report is found, removing...');			
			
			fs.unlink(reportPath, function(err) {				
				if (err) throw err;				
				
				debug && console.log('Report has been removed successfully.');				
				
				var wStream = new fs.WriteStream(reportPath);				
				
				wStream.on('error', function(err) {					
					if (err) throw err;
				});				
				
				wStream.on('finish', function() {
					console.log('Html file was created successfully.');
					console.log('Path: ' + reportPath);
				});				
				
				callback(wStream);					
			});
		} else {
			debug && console.log('There is no report file in the system to be removed!');
		}		
	});
}

function _unitTestHandler(args, isOutputed) {
	
	var outputData = ''
	  , isOutputed = isOutputed === undefined
	  , outputError = '';
	  
	var nodeUnit = spawn('bash', [
		'-c', 'nodeunit ' + args.join(' ')
	], {
		cwd : undefined,
		env : process.env,
		stdio : ['ipc']
	});
	
	nodeUnit.stdout.on('data', function (data) {									
		outputData += data;
	});

	nodeUnit.stderr.on('data', function (data) {				
		outputError += data;		
	});

	nodeUnit.on('close', function (code) {	
		debug && console.log('Code: ' + code);
										
		isOutputed && console.log(outputData);
		isOutputed && console.log(outputError);	
	});		
	
	return nodeUnit;
}

function UnitTest(args) {	
	
	if (~args.indexOf('html')) {
				
		_updateHtml(function(wStream) {
			var nodeUnit = _unitTestHandler(args, false);
			nodeUnit.stdout.pipe(wStream);
		});
	} else {
		_unitTestHandler(args);
	}
	
}

if (module.parent) {
	module.exports = UnitTest;
} else {
	UnitTest(process.argv.slice(2));
}