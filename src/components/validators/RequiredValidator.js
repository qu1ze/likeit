var util = require('util')
    , superClass = require('../Validator.js');

/**
 * @example rule - required: {attributes: [''], message: 'My message', on: ['insert'], except: ['update']},
 */
function RequiredValidator(ruleParams) {
    superClass.call(this, ruleParams);
}

util.inherits(RequiredValidator, superClass);

RequiredValidator.prototype.validateAttribute = function(model, attribute, next) {
	var attributes = model.getAttributes();		
    if(!(attribute in attributes) || typeof attributes[attribute] === 'undefined' || !attributes[attribute])
        if(attributes[attribute] !== 0)
            this.addError(model, attribute)
		
    next();
}

RequiredValidator.prototype.getMessage = function(attribute) {
    return attribute + ' is required!';
}

module.exports = RequiredValidator;