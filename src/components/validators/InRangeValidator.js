var util = require('util')
    , superClass = require('../Validator.js');

/**
 * @example rule - required: {attributes: [''], message: 'My message', on: ['insert'], except: ['update']},
 */
function InRangeValidator(ruleParams) {
    superClass.call(this, ruleParams);
    this.range = ruleParams.range;
}

util.inherits(InRangeValidator, superClass);

InRangeValidator.prototype.validateAttribute = function(model, attribute, next) {
    if (!~this.range.indexOf(model.getAttributes(attribute)))
		this.addError(model, attribute);
		
	next();
}

InRangeValidator.prototype.getMessage = function(attribute) {
    return attribute + ' is not in range (' + this.range.join(', ') + ')!';
}

module.exports = InRangeValidator;