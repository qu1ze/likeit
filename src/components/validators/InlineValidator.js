var util = require('util')
    , superClass = require('../Validator.js');

/**
 * @example rule - required: {attributes: [''], message: 'My message', on: ['insert'], except: ['update']},
 */
function InlineValidator(ruleParams) {
    superClass.call(this, ruleParams);		
	this.method = '';	
}

util.inherits(InlineValidator, superClass);

InlineValidator.prototype.validateAttribute = function(model, attribute, next) {
	this.method && model[this.method](attribute, this.ruleParams, next);
}

module.exports = InlineValidator;