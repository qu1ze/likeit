var underscore = require('underscore');

module.exports = {  
  
  errorResponse : function(sourceErrors) {
    if (sourceErrors instanceof Array) {
      var errors = {};
      
      sourceErrors.forEach(function(item) {
        underscore.extend(errors, item.error);
      });
      
      return errors;
    } else {
      return sourceErrors;
    }
  }
  
};