function WordUtils() {
	this.d = [];
	for (var i=0;i<=100;++i)
		this.d[i] = [];
};

WordUtils.prototype.probablyEqual = function(s, t) {	
	return t && s && (s == t || this.getLevenshteinDistance(s, t) <= Math.max(1, (t.length + 2) / 5) && t.length > 4);
}

WordUtils.prototype.getLevenshteinDistance = function(s, t)
{	
	var m = s.length;
	var n = t.length;
	for (var i=0;i<=m;++i)
    {
		this.d[i][0] = i;
    }
	for (var j=0;j<=n;++j)
	{
		this.d[0][j] = j;
	}
	for (var j=1;j<=n;++j)
	{
		for (var i=1;i<=m;++i)
		{
			if (s[i] == t[j])
			{
				this.d[i][j] = this.d[i-1][j-1];
			}
			else
			{
				this.d[i][j] = 1 + Math.min(this.d[i-1][j],this.d[i][j-1],this.d[i-1][j-1]);
			}
	
		}
	}
	return this.d[m][n];
};

module.exports = new WordUtils();
