var ModelError = require('../../components/errors/ModelError.js');
var attribute = 'Firstname'
  , message = attribute + ' is required';


module.exports = {

	setUp : function(callback) {
		callback();
	},
	tearDown : function(callback) {
		callback();
	},
	general : function(test) {
		test.expect(2);
		
        test.throws(function(){			
			throw new ModelError(attribute, message);
		}, function(err) {									
			test.deepEqual(err, { message : message, attribute : attribute }, 'Error was thrown correctly');			
			return err instanceof ModelError;
		}, 'Throwing of ModelError instance.');
		
        test.done();
	},
	badCall : function(test) {
		test.expect(2);
		
        test.throws(function(){			
			throw new ModelError();			
		}, function(err) {						
			test.strictEqual(err.toString(), 'Error: Empty ModelError params!', 'Error messages are the same');			
			return err instanceof Error;
		}, 'Throwing of ModelError instance with empty params.');
				
		test.done();
	}
};