var AR = require('../../components/ActiveRecord.js')
  , mongodb = require('mongodb')
  , ObjectID = require('mongodb').ObjectID
  , underscore = require('underscore')
  , nconf = require('../../config').nconf
  , MongoClient = mongodb.MongoClient;

function checkDataBaseConnection(test) {
	test.expect(1);
	test.notEqual(this.AR.getConnection(), null, 'DB Connection is OK');	
	test.done();
}

function testInsert(test) {
	
	var self = this
	  , data = {
			a : 1,
			b : '2',				
			d : null,					
			f : function() {},
			g : [],
			h : {}
		}
	  , source = underscore.extend({}, data);	  
	
	//expecting 4 verifications
	test.expect(4);	
	
	//inserting new row
	self.AR.insert([ source ], function(insertErr, result) {		
		test.equal(insertErr, null, 'No errors while inserting. Success!');
		
		//finding the row
		self.AR
			.getConnection()
			.collection(self.AR.tableName())
			.findOne({ a : 1 }, function(findOneError, item) {
				test.equal(findOneError, null, 'No errors while using findOne for getting inserted row');
				test.equal(item.f.code, 'function () {}', 'Function was inserted successfully');
							
				item.f = data.f = null;
				delete item._id;
				
				test.deepEqual(item, data, 'Row was inserted successfully');
				test.done();
			}); 
		
	}, {
		serializeFunctions : true
	});					
		
}

function testUpdate(test) {
	var self = this
	  , data = {
			a : 1			
		}
	  , dataCopy = underscore.extend({}, data)
	  , dataForUpdate = {
			a : 2
		};
		
	var connection = self.AR.getConnection().collection(self.AR.tableName());
	
	//expecting 7 verifications
	test.expect(7);
	
	//inserting new row
	self.AR.insert([ data ], function(insertErr, result) {
		test.equal(insertErr, null, 'No errors while inserting. Success!');
						
			//finding inserted row
			connection.findOne(dataCopy, function(findOneError, item) {
				test.equal(findOneError, null, 'No errors while using findOne for getting inserted row');
								
				test.strictEqual(item.a, dataCopy.a, 'Row was inserted successfully');
				
				//updating the row
				self.AR.update(dataCopy, dataForUpdate, function(updateError, updated) {
					test.equal(updateError, null, 'No errors while updating');	
					test.equal(updated, 1, 'Successfull updated 1 row');	
					
					//finding updated row
					connection.findOne(dataForUpdate, function(findOneError, updatedItem) {
						test.equal(findOneError, null, 'No errors while using findOne for getting UPDATED row');																		
						test.strictEqual(updatedItem.a, dataForUpdate.a, 'Row was updated successfully');						
						test.done();
					});									
				});							
			}); 		
	}, {
		serializeFunctions : true
	});	
}

function testDeleteByAttributes(test) {
	var self = this
	  , data = [{ field : 1 }, { field : 2 }, { field : 3 }, { field : 1 }]
	  , connection = self.AR.getConnection().collection(self.AR.tableName());	  
	
	//expecting 4 verifications
	test.expect(7);
	
	//inserting new row
	self.AR.insert(data, function(insertErr, result) {		
		test.equal(insertErr, null, 'No errors while inserting. Success!');
		
		//finding the row
		connection.find().toArray(function(findError, items) {
			
			test.equal(findError, null, 'No errors while using find for getting inserted rows');				
			test.ok(items.length == 4, 'Rows was inserted successfully');			
			self.AR.deleteByAttributes({ field : 1 }, function(deleteError, removedItems) {
				
				test.equal(deleteError, null, 'No errors while deleting the row');
				test.equal(removedItems, 1, 'Removing 1 row');				
				connection.findOne({ field : 1 }, function(findError, findResult) {
					
					test.equal(findError, null, 'No errors while finding any records');
					test.ok(findResult.field == 1 && findResult._id, 'There is still 1 row');
					test.done();
				});				
			});			
		}); 				
	}, {});					
		
}

function testDeleteByPk(test) {
  var self = this
    , objID = new ObjectID()
	  , data = [{ _id : objID, field : 1 }]
	  , connection = self.AR.getConnection().collection(self.AR.tableName());	  
    
  //expecting 5 verifications
	test.expect(5);
	
	//inserting new row
	self.AR.insert(data, function(insertErr, result) {		
		test.equal(insertErr, null, 'No errors while inserting. Success!');
		
		//finding the row
		connection.find().toArray(function(findError, items) {
			
			test.equal(findError, null, 'No errors while using find for getting inserted rows');				
			test.ok(items.length == 1, 'Rows was inserted successfully');			
			self.AR.deleteByPk(objID, function(deleteError, removedItems) {				
				test.equal(deleteError, null, 'No errors while deleting the row');
				test.equal(removedItems, 1, 'Removing 1 row');
        test.done();
			});			
		}); 				
	}, {});					
}
  
function testDeleteAllByAttributes(test) {
	var self = this
	  , data = [{ field : 1 }, { field : 2 }, { field : 3 }, { field : 1 }]
	  , connection = self.AR.getConnection().collection(self.AR.tableName());	  
	
	//expecting 4 verifications
	test.expect(7);
	
	//inserting new row
	self.AR.insert(data, function(insertErr, result) {		
		test.equal(insertErr, null, 'No errors while inserting. Success!');
		
		//finding the row
		connection.find().toArray(function(findError, items) {
			
			test.equal(findError, null, 'No errors while using find for getting inserted rows');				
			test.ok(items.length == 4, 'Rows was inserted successfully');	
			
			self.AR.deleteAllByAttributes({ $or : [{ field : 1 }, { field : 2 }, { field : 3 }] }, function(deleteError, removedItems) {
				
				test.equal(deleteError, null, 'No errors while deleting the rows');
				test.equal(removedItems, 4, 'Removing 4 rows');				
				connection.find().toArray(function(findError, findResult) {
					
					test.equal(findError, null, 'No errors while finding any records');
					test.equal(findResult.length, 0, 'No results in the collection');
					test.done();
				});				
			});			
		}); 
	}, {});							
}

function testDeleteAll(test) {
	var self = this
	  , data = [{ field : 1 }, { field : 2 }, { field : 3 }, { field : 1 }]
	  , connection = self.AR.getConnection().collection(self.AR.tableName());	  
	
	//expecting 4 verifications
	test.expect(7);
	
	//inserting new row
	self.AR.insert(data, function(insertErr, result) {		
		test.equal(insertErr, null, 'No errors while inserting. Success!');
		
		//finding the row
		connection.find().toArray(function(findError, items) {
			
			test.equal(findError, null, 'No errors while using find for getting inserted rows');				
			test.ok(items.length == 4, 'Rows was inserted successfully');	
			
			self.AR.deleteAll(function(deleteError, removedItems) {
				
				test.equal(deleteError, null, 'No errors while deleting the rows');
				test.equal(removedItems, 4, 'Removing 4 rows');				
				connection.find().toArray(function(findError, findResult) {
					
					test.equal(findError, null, 'No errors while finding any records');
					test.equal(findResult.length, 0, 'No results in the collection');
					test.done();
				});				
			});			
		}); 
	}, {});							
}

function testSave(test) {
	var model = new AR();
	model.setAttributes({
		firstname : 'Oleg',
		email : 'qu1ze34@gmail.com'
	});
	model.tableName = function() { return 'testUser' };
	
	test.expect(6);
	
	model.save(function(saveError, saveResult) {
		test.equal(saveError, null, 'No errors while saving');
		test.ok(model.getAttributes('_id'), 'Model was successfully saved to DB');		
		
		model.setAttributes({
			email : 'qu1ze@mail.ru'
		});
		
		model.save(function(saveError, saveResult) {
			test.equal(saveError, null, 'No errors while updating');
			test.equal(saveResult, 1, 'Row was updated!');
			test.equal(model.getAttributes('firstname'), 'Oleg', 'Firstname is the same');
			test.equal(model.getAttributes('email'), 'qu1ze@mail.ru', 'Email has been successfully updated');
			
			test.done();
		});
			
	});	
}

function insertSomeData(test, callback) {
	var data = [
		{ a : 1, b : 2, c : 3, d : 10 },
		{ _id : 12345678, a : 1, b : 2, c : 3, d : 15 },
		{ a : 2, b : 3, c : 3 },
		{ a : 1, b : 3, c : 2, s : 'string' },		
		{ a : 5, b : 3, c : 5, s : 'string string' },		
	];
	this.AR.insert(data, function(insertErr, result) {	
		test.equal(insertErr, null, 'No errors while inserting some data');
		callback();
	});
}

function testFind(test) {
	var self = this;
	test.expect(3);
	insertSomeData.call(self, test, function() {
		self.AR.find({$or : [{a : 1}, {a : 2}]}, function(err, result) {
			test.equal(err, null, 'No errors while find');
			test.equal(result.length, 4, 'Data was successfully found');
			test.done();
		});		
	});	
}

function testFindAll(test) {
	var self = this;
	test.expect(3);
	insertSomeData.call(self, test, function() {
		self.AR.findAll(function(err, result) {
			test.equal(err, null, 'No errors while findAll');
			test.equal(result.length, 5, 'Data was successfully found');
			test.done();
		});		
	});	
}

function testFindByAttributes(test) {
	var self = this;
	test.expect(3);
	insertSomeData.call(self, test, function() {
		self.AR.findByAttributes({ a : 1 }, function(err, result) {
			test.equal(err, null, 'No errors while findAll');			
			test.deepEqual(result.getAttributes(['a', 'b', 'c']), { a : 1, b : 2, c : 3 }, 'Data was successfully found');			
			test.done();
		});		
	});		
}

function testFindAllByAttributes(test) {
	var self = this;
	test.expect(3);
	insertSomeData.call(self, test, function() {		
		self.AR.findAllByAttributes({ a : 1 }, function(err, result) {			
			test.equal(err, null, 'No errors while findAll');			
			test.equal(result.length, 3, 'Data was successfully found');			
			test.done();
		});		
	});		
}

function testAggregate(test) {		
	var self = this;
	test.expect(4);
	insertSomeData.call(self, test, function() {		
		self.AR.aggregate([
		{ $match : { b : 2 } },
		{
			$group : {
				_id : null,
				sum : { $sum : '$d' }
			}
		}
		], function(err, result) {			
			test.equal(err, null, 'No errors while aggregating');			
			test.equal(result.length, 1, 'Data was successfully found');		
			test.equal(result[0].sum, 25, 'Aggregation Summary is correct')						
			test.done();
		}, {});		
	});			
}

function testCountAll(test) {
	var self = this;
	test.expect(3);
	insertSomeData.call(self, test, function() {
		self.AR.countAll(function(err, count) {
			test.equal(err, null, 'No Errors while countAll');
			test.equal(count, 5, 'Count is correct');
			test.done();
		});
	});
}

function testCountAllByAttributes(test) {
	var self = this;
	test.expect(5);
	insertSomeData.call(self, test, function() {
		self.AR.countAllByAttributes({ b : 3 }, function(err, count) {
			test.equal(err, null, 'No Errors while countAllByAttributes');
			test.equal(count, 3, 'Count is correct');
			
			self.AR.countAllByAttributes({ b : 3 }, function(err, count) {
				test.equal(err, null, 'No Errors while countAllByAttributes');
				test.equal(count, 1, 'Count with Limit is correct');
				test.done();
			}, { limit : 1 });					
		});
	});
}

function testDistinctAll(test) {
	var self = this;
	test.expect(3);
	insertSomeData.call(self, test, function() {
		self.AR.distinctAll('a', function(err, results) {
			test.equal(err, null, 'No Errors while distinctAll');
			test.deepEqual(results.sort(), [1, 2, 5], 'Distinct is correct');
			test.done();
		});
	});
}

function testDistinctAllByAttributes(test) {
	var self = this;
	test.expect(3);
	insertSomeData.call(self, test, function() {
		self.AR.distinctAllByAttributes('c', { a : 1 }, function(err, results) {
			test.equal(err, null, 'No Errors while distinctAllByAttributes');
			test.deepEqual(results.sort(), [2, 3], 'Distinct by attributes is correct');
			test.done();
		});
	});
}

function testDbRef(test) {
	var self = this
	  , userID = new ObjectID()
	  , userData = { _id : userID, name : 'Oleg' };
		
	test.expect(5);
	self.AR.insert(userData, function(err, result) {
		test.equal(null, err, 'No errors while inserting');
		self.AR
			.getConnection()
			.collection('posts')
		    .insert({ user : new mongodb.DBRef('test', userID, null) }, {}, function(err, item) {
				test.equal(null, err, 'No errors while inserting 2');
				self.AR
					.getConnection()
					.collection('posts').find().toArray(function(err, results) {
						test.equal(null, err, 'No errors while finding');
						self.AR
						.getConnection()
						.dereference(results[0].user, function(err, item) {
							test.equal(err, null, 'No errors while dereferencing');
							test.deepEqual(item, userData, 'Dereferencing user data');
							test.done();
						});
					});
			});
	});	 		
}

function run() {
	
	var exports = {
		setUp : function(callback) {
			var self = this
			  , testConnectionString = nconf.get('testdb:connectionString');
			
			console.log('Connection to database...' + testConnectionString);				
			//creating db connection
			MongoClient.connect(testConnectionString, function(mongoConnectionError, db) {
				if (mongoConnectionError) {
					console.log('Mongo Connection Error');
					throw mongoConnectionError;  
				}
					
				console.log('Connected successfully...');	
				
				global.mongoDb = db;
				self.AR = new AR();			
				self.AR.tableName = function() { return 'test' };
				callback();		
			});			
		},
		tearDown : function(callback) {
			this.AR = {};
			//removing db connection with dropping database
			if (global.mongoDb) {
				global.mongoDb.dropDatabase(function(err, result) {
					global.mongoDb.close();
					callback();
				});							
			} else {
				callback();
			}
		},		
		checkDataBaseConnection : checkDataBaseConnection,
    testFind : testFind,
    testDeleteByPk : testDeleteByPk,
		testDbRef : testDbRef,
		testDistinctAll : testDistinctAll,
		testDistinctAllByAttributes : testDistinctAllByAttributes,
		testCountAll : testCountAll,
		testCountAllByAttributes : testCountAllByAttributes,
		testInsert : testInsert,
		testUpdate : testUpdate,
		testDeleteByAttributes : testDeleteByAttributes,
		testDeleteAllByAttributes : testDeleteAllByAttributes,
		testDeleteAll : testDeleteAll,
		testFindAll : testFindAll,
		testFindByAttributes : testFindByAttributes,
		testFindAllByAttributes : testFindAllByAttributes,		
		testSave : testSave,
		testAggregate : testAggregate
	};					
			
	return exports;
}

module.exports = run();

	