var Model = require('../../components/Model.js');

module.exports = {

	setUp : function(callback) {		
		this.model = new Model('insert');
		this.model.setAttributes({
			firstname : 'Oleg',
			lastname : 'Meleshko'
		});
		this.model.rules = function() {
			return [
				{
					rule : 'required',
					attributes: ['firstname', 'lastname'], 
					message: 'Firstname and lastname should be filled in', 
					on: ['insert'], 
					except: ['update']
				}
			];
		}		
		callback();
	},
	tearDown : function(callback) {
		this.model = {};
		callback();
	},
	successValidations : function(test) {		
		test.expect(1);						
		this.model.validate(function(result) {
			test.ok(result, 'Validation passed');
			test.done();
		});								
	},
    failedValidations : function(test) {       
		test.expect(4);							
		var self = this;
		this.model.unsetAttributes();
		this.model.setAttributes({
			firstname : 'Oleg'		
		});	
		this.model.validate(function(result) {
			test.equal(result, false, 'Failed validation (no required attribute in the model)');
			
			self.model.setAttributes({
				firstname : 'Oleg',
				lastname : ''
			});		
			
			self.model.validate(function(result) {
				test.equal(result, false, 'Pass Failed validation (empty)');
				
				self.model.setAttributes({
					firstname : 'Oleg',
					lastname : null
				});		
				self.model.validate(function(result) {
					test.equal(result, false, 'Pass Failed validation (null)');
					
					self.model.setAttributes({
						firstname : 'Oleg',
						lastname : undefined
					});		
					self.model.validate(function(result) {
						test.equal(result, false, 'Pass Failed validation (undefined)');
						test.done();
					});									
				});								
			});			
		});			
    },
	failedValidationExcept : function(test) {
		test.expect(1);
		this.model.setScenario('update');
		this.model.validate(function(result) {
			test.ok(result, 'Another scenario');
			test.done();
		});		
	}
};