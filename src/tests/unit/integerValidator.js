var Model = require('../../components/Model.js')
  , underscore = require('underscore');

module.exports = {

	setUp : function(callback) {		
		this.model = new Model('insert');
		this.model.setAttributes({
			a : 1, b : '2', c : '1.0', d : 1.0, e : 0, f : 0.0, g : -1, h : -1.0,
			i : '-1', j : '-1.0', k : 255, l : 1000000000
		});
		this.model.rules = function() {
			return [
				{
					rule : 'integer',
					attributes: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'],
					on: ['insert'], 
					except: ['update']
				}
			];
		}		
		callback();
	},
	tearDown : function(callback) {
		this.model = {};
		callback();
	},
	successValidation : function(test) {		
		test.expect(1);						
		this.model.validate(function(result) {
			test.ok(result, 'Pass validation');			
			test.done();
		});			
	},
    failedValidations : function(test) {       
		test.expect(2);	
		var self = this;
		this.model.unsetAttributes();
		this.model.setAttributes({
			a : true, b : false, c : 'true', d : 'false', e : null, f : [], g : {}, h : NaN,
			i : Infinity, j : undefined	, k : -Infinity, l : 2.51
		});	
		this.model.validate(function(result) {
			test.equal(result, false, 'Failed validation passed');		
			test.equal(underscore.values(self.model.getErrors()).length, 12, 'All fields are successfully wrong!');
			test.done();
		});	
    },	
};