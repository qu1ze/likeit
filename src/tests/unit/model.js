var underscore = require('underscore') 
  , Model = require('../../components/Model.js');

var attributes = {
	firstname : 'Oleg',
	email : 'qu1ze34@gmail.com'
};

module.exports = {

	setUp : function(callback) {
		this.model = new Model();
		callback();
	},
	tearDown : function(callback) {
		this.model = {};
		callback();
	},
	testAttributes : function(test) {
		test.expect(3);
				
		this.model.setAttributes(attributes);
		test.deepEqual(attributes, this.model.getAttributes(), 'Attributes have been added successfully');
		
		this.model.unsetAttributes();
		test.deepEqual(this.model.getAttributes(), {}, 'Attributes have been removed successfully');
		
		this.model.setAttributes(attributes);
		this.model.unsetAttributes(['email']);
		test.deepEqual(this.model.getAttributes(), {firstname : 'Oleg', email : null}, 'Attribute has been removed successfully');
		
		test.done();
	},
	gettingAttributes : function(test) {
		test.expect(6);
		this.model.setAttributes(attributes);
		test.deepEqual(this.model.getAttributes(), attributes, 'Attributes have been added successfully');		
		test.equal(this.model.getAttributes('firstname'), attributes.firstname, 'Getting single attribute is correct');		
		test.equal(this.model.getAttributes('email'), attributes.email, 'Getting single attribute is correct');		
		test.deepEqual(this.model.getAttributes(['email']), { email : attributes.email }, 'Getting single attribute as array is correct');		
		test.deepEqual(this.model.getAttributes(['firstname']), { firstname : attributes.firstname }, 'Getting single attribute as array is correct');		
		test.deepEqual(this.model.getAttributes(['firstname', 'email']), attributes, 'Getting few attributes is correct');		
		
		test.done();
	},
	testErrors : function(test) {
		test.expect(13);		
		this.model.addError('firstname', 'firstname is not valid!');
		test.ok(this.model.hasErrors(), 'hasErrors without attribute');
		test.ok(this.model.hasErrors('firstname'), 'hasErrors with attribute (success)');
		test.ok(!this.model.hasErrors('lastname'), 'hasErrors with attribute (failed)');
		
		test.equal(this.model.getError(), null, 'Empty getError');
		test.deepEqual(this.model.getError('firstname'), ['firstname is not valid!'], 'getError firstname');
		test.equal(this.model.getError('lastname'), null, 'getError lastname (failed)');
		
		test.deepEqual(this.model.getErrors(), { firstname : ['firstname is not valid!'] }, 'Get Errors');
		test.equal(this.model.getErrors('firstname').length, 1, 'Get Errors length');
		test.equal(this.model.getErrors('lastname').length, 0, 'Get Errors length (failed)');
		
		this.model.addErrors([
			{ attribute : 'year', messages : ['year is not valid'] },
			{ attribute : 'size', messages : ['size is not valid', 'size is too small'] }
		]);
		
		test.equal(underscore.toArray(this.model.getErrors()).length, 3, 'All errors in set');	
		test.deepEqual(this.model.pullErrors('year'), ['year is not valid'], 'Pull errors for year field');
		test.equal(underscore.toArray(this.model.pullErrors()).length, 2, 'Pull all errors');
		test.ok(!underscore.toArray(this.model.getErrors()).length, 'No errors');
		test.done();
	}
};