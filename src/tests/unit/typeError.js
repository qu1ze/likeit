var TypeError = require('../../components/errors/TypeError.js')
  , expectedClass = Error
  , givenClass = [].constructor;

module.exports = {

	setUp : function(callback) {
		callback();
	},
	tearDown : function(callback) {
		callback();
	},
	general : function(test) {
		test.expect(2);
		
        test.throws(function(){
			if (!(Error instanceof [].constructor))
				throw new TypeError(givenClass, expectedClass);
		}, function(err) {						
			test.strictEqual(err.toString(), 'TypeError: Expected ' + expectedClass + ', but ' + givenClass + ' was given!', 'Error messages are the same');
			
			return err instanceof TypeError;
		}, 'Throwing of TypeError instance.');
		
        test.done();
	},
	badCalls : function(test) {
		test.expect(4);
		
        test.throws(function(){			
			throw new TypeError();			
		}, function(err) {						
			test.strictEqual(err.toString(), 'Error: Expected class name or given class name is undefined', 'Error messages are the same');			
			return err instanceof Error;
		}, 'Throwing of TypeError instance with empty params.');
		
		test.throws(function(){			
			throw new TypeError(Error, Error);			
		}, function(err) {						
			test.strictEqual(err.toString(), 'Error: Expected and given classes are the same', 'Error messages are the same');			
			return err instanceof Error;
		}, 'Throwing of TypeError instance with the same params');
		
        test.done();
	}
};