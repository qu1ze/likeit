var Model = require('../../components/Model.js');

module.exports = {

	setUp : function(callback) {		
		this.model = new Model('insert');
		this.model.setAttributes({
			login : 'login',			
		});
		this.model.customValidator = function(attribute, ruleParams, next) {
			var attributeValue = this.getAttributes(attribute);
			if (attributeValue.length <= 3 || attributeValue.length >= 10)
				this.addError(this.model, attribute);
				
			next();
		}	
		this.model.rules = function() {
			return [
				{
					rule : 'customValidator',
					attributes: ['login'], 
					message: 'Login length should be > 3 and < 10'					
				}
			];
		}		
		callback();
	},
	tearDown : function(callback) {
		this.model = {};
		callback();
	},
	successValidations : function(test) {		
		test.expect(1);						
		this.model.validate(function(result) {
			test.ok(result, 'Passing correct model field');		
			test.done();
		});									
	},
    failedValidations : function(test) {       
		test.expect(1);							

		this.model.setAttributes({
			login : 'log'		
		});	
		
		this.model.validate(function(result) {			
			test.equal(result, false, 'Passing wrong model field.');
			test.done();
		});		       
    }	
};