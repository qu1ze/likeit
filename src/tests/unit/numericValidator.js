var Model = require('../../components/Model.js')
  , underscore = require('underscore');

module.exports = {

	setUp : function(callback) {		
		this.model = new Model('insert');
		this.model.setAttributes({
			a : Infinity, b : -Infinity, c : '1.0', d : 1.0, e : 0, f : 0.0, g : -1, h : -1.0,
			i : '-1', j : '-1.0', k : 255.5, l : '255.5', m : '-255.5', n : -255.53, o : .45, p : -.45
		});
		this.model.rules = function() {
			return [
				{
					rule : 'numeric',
					attributes: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p']			
				}
			];
		}		
		callback();
	},
	tearDown : function(callback) {
		this.model = {};
		callback();
	},
	successValidation : function(test) {		
		test.expect(1);						
		this.model.validate(function(result) {
			test.ok(result, 'Pass validation');	
			test.done();
		});				
	},
    failedValidations : function(test) {       
		test.expect(2);		
		var self = this;
		this.model.unsetAttributes();
		this.model.setAttributes({
			a : true, b : false, c : 'true', d : 'false', e : null, f : [], g : {}, h : NaN,
			i : [1], j : undefined, k : -undefined, l : -{}, m : [[]], n : [0], o : function(){} , p : -NaN
		});	
		this.model.validate(function(result) {
			test.equal(result, false, 'Failed validation passed');
			test.equal(underscore.values(self.model.getErrors()).length, 16, 'All fields are successfully wrong!');
			test.done();
		});						
    },	
};