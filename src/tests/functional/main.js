casper.test.begin('Demo Login Test', 9, function(test) {
	casper.start('http://localhost:3000/', function() {
		test.assertTextExists('Sign in', 'Sign in button is here!');
		
		var formId = 'form#loginForm';
		
		test.assertElementCount(formId, 1);
		test.assertElementCount('input[name="email"]', 1);
		test.assertElementCount('input[name="password"]', 1);				
		
		this.fillSelectors(formId, {
			'input[name="email"]' : 'admin',
			'input[name="password"]' : 'admin'
		}, true);						
	});
	
	casper.then(function() {
		test.assertTextExists('Logout', 'User has been logged in!');				
	});	
	
  casper.thenOpen('http://localhost:3000/site/user', function() {
    test.assertTextExists('Success for authorized user!', 'Success for authorized user! (Password filter isAuthenticated)');
  });
  
  casper.thenOpen('http://localhost:3000/', function() {
    test.assertTextExists('Logout', 'User has been logged in!');	
    
    this.click('#logoutBtn');
  });
  
	casper.then(function() {
		test.assertTextExists('Sign in', 'User has been logged out!');
	});
  
  casper.thenOpen('http://localhost:3000/site/user', function() {
    test.assertTextDoesntExist('Logout', 'Forbidden successfull');	
  });
	
	casper.run(function() {
		test.done();
	});
});